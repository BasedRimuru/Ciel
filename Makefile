#This file is part of Ciel.

#Ciel is free software: you can redistribute it and/or modify it
#under the terms of the GNU General Public License as published
#by the Free Software Foundation, version 3.

#Ciel is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
#See the GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with Ciel. If not, see <https://www.gnu.org/licenses/>.

DEBUG ?= 0

PWD = $(shell pwd)
BUILDDIR = $(PWD)/build
OBJDIR = $(BUILDDIR)/objects
EXTDIR = $(BUILDDIR)/external

CFLAGS += -Wall -Wextra --std=gnu99 -I$(PWD)/include
CFLAGS += -ffreestanding -m32 -nostdlib

ifeq ($(DEBUG), 1)
    CFLAGS += -Og -ggdb3
else
    CFLAGS += -s -O2
endif

export PWD
export BUILDDIR
export OBJDIR
export CFLAGS

.PHONY: all clean depclean inshdr cleanhdr asm utils

all: $(BUILDDIR)/ciel.iso
clean:
	rm -rf build
depclean: clean
	rm -rf external/init.bin
	rm -rf external/libtempest.a external/Tempest
inshdr:
	mkdir -p /usr/include/ciel/
	cp include/ciel/*.Slime /usr/include/ciel/
cleanhdr:
	rm -rf /usr/include/ciel/


$(BUILDDIR)/ciel.bin: src/ciel/linker.ld asm utils $(OBJDIR)/init.o \
                      $(OBJDIR)/loader.o external/libtempest.a | $(BUILDDIR)
	gcc $(CFLAGS) -o $@ -T $< $(OBJDIR)/* -Lexternal/ -ltempest
ifeq ($(DEBUG), 1)
$(BUILDDIR)/ciel.elf: src/ciel/linker.ld asm utils $(OBJDIR)/init.o \
                      $(OBJDIR)/loader.o external/libtempest.a | $(BUILDDIR)
	gcc $(CFLAGS) -o $@ -T $< $(OBJDIR)/* -Lexternal/ -ltempest
$(BUILDDIR)/ciel.iso: $(BUILDDIR)/ciel.bin $(BUILDDIR)/ciel.elf grub/grub.cfg \
                      | $(BUILDDIR)
	mkdir -p build/iso/boot/grub
	cp $< build/iso/boot/
	cp grub/grub.cfg build/iso/boot/grub/
	grub-mkrescue --compress xz -o $@ build/iso
else
$(BUILDDIR)/ciel.iso: $(BUILDDIR)/ciel.bin grub/grub.cfg \
                      | $(BUILDDIR)
	mkdir -p build/iso/boot/grub
	cp $< build/iso/boot/
	cp grub/grub.cfg build/iso/boot/grub/
	grub-mkrescue --compress xz -o $@ build/iso
endif


asm:     | $(OBJDIR)
	@$(MAKE) -C src/ciel/$@
utils:   | $(OBJDIR)
	@$(MAKE) -C src/ciel/$@


$(OBJDIR)/init.o: external/init.bin | $(OBJDIR)
	cd external && ld -m elf_i386 -r -b binary -o $@ init.bin
$(OBJDIR)/loader.o: src/ciel/loader.c | $(OBJDIR)
	gcc $(CFLAGS) -mgeneral-regs-only -Ibuild -Iinclude -c -o $@ $<


external/init.bin:
	$(error Needs external/init.bin to continue)
external/libtempest.a: external/Tempest
	cd external/Tempest && make build/libtempest.a
	cp external/Tempest/build/libtempest.a $@
external/Tempest:
	cd external && git clone https://gitlab.com/BasedRimuru/Tempest


$(BUILDDIR):
	mkdir -p $@
$(OBJDIR): | $(BUILDDIR)
	mkdir -p $@
$(EXTDIR): | $(BUILDDIR)
	mkdir -p $@
