# Ciel
Ciel is a cooperative multitasking, single address space, ring 0 only,
nanokernel written for x86.

It runs Tempest bytecode tasks, giving multiprocess control,
access to x86 IO instructions, interrupt handling, and native calls
to virtual code.

## Project status
The project is now on beta, unless necessary, no new features will be added.

## Example
```assembly
jmp main

data:
    .string: .string nt "Just kidding, you should give me a task."
    ..:
..:

main:
    mov r0 data.string
    int ciel.syscall.bsod

    int ciel.syscall.quit
..:
```

```shell
cat include/ciel.Slime example.Slime > external/init.Slime
tempest-as external/init.Slime external/init.bin
make
```
