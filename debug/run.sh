#!/bin/sh

export DEBUG=1
make clean && make all && (qemu-system-i386 -s -S build/ciel.iso &) \
&& sleep 1 && gdb --command=debug/gdb.script
