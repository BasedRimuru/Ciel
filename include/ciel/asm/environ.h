/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CIEL_ASM_ENVIRON_H
#define CIEL_ASM_ENVIRON_H

#include "ciel/utils/types.h"

extern u64 __gdt[3];
extern u16 __gdtr[3];
extern u8 __idt[4096];
extern u16 __idtr[3];
void __load_gdtr(void);
void __load_idtr(void);
void __segrel(void);
void __int(u16 i);

void __int86(u16 i, void *r);
extern u8 __buf86[1024];

#endif
