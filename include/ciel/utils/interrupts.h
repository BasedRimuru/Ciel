/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CIEL_UTILS_INTERRUPTS_H
#define CIEL_UTILS_INTERRUPTS_H

#include "ciel/utils/types.h"

void interrupts_enable(void);
void interrupts_disable(void);
void interrupts_handle(u16 val, void *isr);
void interrupts_default(void *unused);

#endif
