/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CIEL_UTILS_IO_H
#define CIEL_UTILS_IO_H

#include "ciel/utils/types.h"

/* Input functions */

static inline u8
__in8(u16 port)
{
    u8 ret = 0;
    asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

static inline u16
__in16(u16 port)
{
    u16 ret = 0;
    asm volatile ("inw %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

static inline u32
__in32(u16 port)
{
    u32 ret = 0;
    asm volatile ("inl %1, %0" : "=a"(ret) : "Nd"(port));
    return ret;
}

/* Output functions */

static inline void
__out8(u8 val, u16 port)
{
    asm volatile ("outb %0, %1" :: "a"(val), "Nd"(port));
}

static inline void
__out16(u16 val, u16 port)
{
    asm volatile ("outw %0, %1" :: "a"(val), "Nd"(port));
}

static inline void
__out32(u32 val, u16 port)
{
    asm volatile ("outl %0, %1" :: "a"(val), "Nd"(port));
}

#endif
