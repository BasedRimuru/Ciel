/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#ifndef CIEL_UTILS_TYPES_H
#define CIEL_UTILS_TYPES_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#include "tempest/core.h"

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#endif
