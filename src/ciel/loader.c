/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#include "ciel/asm/environ.h"
#include "ciel/utils/io.h"
#include "ciel/utils/screen.h"
#include "ciel/utils/types.h"
#include "ciel/utils/interrupts.h"

#include "tempest/core.h"

extern u8 _binary_init_bin_start[];
extern u8 _binary_init_bin_end[];

enum syscalls
{
    SYSCALL_QUIT = 0,
    SYSCALL_BSOD,

    SYSCALL_DEBUG,

    SYSCALL_SPAWN,
    SYSCALL_YIELD,
    SYSCALL_KILL,

    SYSCALL_IN8,
    SYSCALL_IN16,
    SYSCALL_IN32,
    SYSCALL_OUT8,
    SYSCALL_OUT16,
    SYSCALL_OUT32,

    SYSCALL_CLI,
    SYSCALL_STI,
    SYSCALL_ISR,
    SYSCALL_INT,

    SYSCALL_REAL,
    SYSCALL_NATIVE,
};

static struct tempest tasks[4096] = {0};
static u16 task_i = 0;
static u16 task_c = 1;

static u32 interrupts[256] = {0};

static enum tempest_status
run_syscall(struct tempest *task)
{
    enum tempest_status st = TEMPEST_OK;

    switch(task->intr)
    {
        case SYSCALL_QUIT:
            if (task->ext != (void*)4096)
                task_i++;
            st = TEMPEST_HALT;
            break;
        case SYSCALL_BSOD:
            screen_bsod((char*)task->regs[TEMPEST_REG_R0]);
            break;

        case SYSCALL_DEBUG:
            screen_debug(task, (char*)task->regs[TEMPEST_REG_R0]);
            break;

        case SYSCALL_SPAWN:
            if (task_c < 4096)
            {
                tasks[task_c].size = 0xFFFFFFFF;
                tasks[task_c].regs[TEMPEST_REG_PC] =
                        task->regs[TEMPEST_REG_R0];
                task->regs[TEMPEST_REG_R0] = task_c;
                task_c++;
            }
            else
            {
                for (u16 i = 0; i < 4096; i++)
                {
                    if (tasks[i].size == 0)
                    {
                        tasks[i].regs[TEMPEST_REG_PC] =
                           task->regs[TEMPEST_REG_R0];
                        task->regs[TEMPEST_REG_R0] = i;
                        break;
                    }
                }
            }
            break;
        case SYSCALL_YIELD:
            task_i++;
            break;
        case SYSCALL_KILL:
            tasks[task->regs[TEMPEST_REG_R0] % 4096].size = 0;
            break;

        case SYSCALL_IN8:
            task->regs[TEMPEST_REG_R0] = __in8(task->regs[TEMPEST_REG_R0]);
            break;
        case SYSCALL_IN16:
            task->regs[TEMPEST_REG_R0] = __in16(task->regs[TEMPEST_REG_R0]);
            break;
        case SYSCALL_IN32:
            task->regs[TEMPEST_REG_R0] = __in32(task->regs[TEMPEST_REG_R0]);
            break;
        case SYSCALL_OUT8:
            __out8(task->regs[TEMPEST_REG_R0], task->regs[TEMPEST_REG_R1]);
            break;
        case SYSCALL_OUT16:
            __out16(task->regs[TEMPEST_REG_R0], task->regs[TEMPEST_REG_R1]);
            break;
        case SYSCALL_OUT32:
            __out32(tasks->regs[TEMPEST_REG_R0], task->regs[TEMPEST_REG_R1]);
            break;

        case SYSCALL_CLI:
            interrupts_disable();
            break;
        case SYSCALL_STI:
            interrupts_enable();
            break;
        case SYSCALL_ISR:
            interrupts[task->regs[TEMPEST_REG_R1] % 256] =
                       task->regs[TEMPEST_REG_R0];
            break;
        case SYSCALL_INT:
            __int(task->regs[TEMPEST_REG_R0]);
            break;

        case SYSCALL_REAL:
            __int86(task->regs[TEMPEST_REG_R0],
                    (void*)task->regs[TEMPEST_REG_R1]);
            break;

        case SYSCALL_NATIVE:
            st = ((enum tempest_status(*)(struct tempest *))
                  (task->regs[TEMPEST_REG_R0]))(task);
            break;
    }

    return st;
}

static void
run_task(struct tempest *task)
{
    if (task->size != 0)
    {
        enum tempest_status st = tempest_run(task);
        if (st == TEMPEST_HALT)
            st = run_syscall(task);

        if (st != TEMPEST_OK)
            task->size = 0;
    }
}

#define REPEAT16(x, n) \
x(n##0) \
x(n##1) \
x(n##2) \
x(n##3) \
x(n##4) \
x(n##5) \
x(n##6) \
x(n##7) \
x(n##8) \
x(n##9) \
x(n##A) \
x(n##B) \
x(n##C) \
x(n##D) \
x(n##E) \
x(n##F)

#define REPEAT256(x) \
REPEAT16(x, 0) \
REPEAT16(x, 1) \
REPEAT16(x, 2) \
REPEAT16(x, 3) \
REPEAT16(x, 4) \
REPEAT16(x, 5) \
REPEAT16(x, 6) \
REPEAT16(x, 7) \
REPEAT16(x, 8) \
REPEAT16(x, 9) \
REPEAT16(x, A) \
REPEAT16(x, B) \
REPEAT16(x, C) \
REPEAT16(x, D) \
REPEAT16(x, E) \
REPEAT16(x, F)

#define DEFINE_ISR(n) \
void __attribute__((interrupt)) \
interrupt##n(void *unused) \
{ \
    (void)unused; \
    struct tempest task = {0}; \
    task.regs[TEMPEST_REG_PC] = interrupts[0x##n]; \
    if (task.regs[TEMPEST_REG_PC] != 0) \
    { \
        task.size = 0xFFFFFFFF; \
        task.ext = (void*)4096; \
        while (task.size != 0) \
            run_task(&task); \
    } \
}

REPEAT256(DEFINE_ISR)

static void __attribute__((noreturn))
run_init(void)
{
    u8 *init = (u8*)0x1100000;
    u16 pc = 0;

    *((u32*)(&(init[pc]))) = (u32)tasks;
    pc += sizeof(u32);
    *((u16*)(&(init[pc]))) = 4096;
    pc += sizeof(u16);
    *((u16*)(&(init[pc]))) = sizeof(struct tempest);
    pc += sizeof(u16);

    *((u32*)(&(init[pc]))) = (u32)&(task_i);
    pc += sizeof(u32);
    *((u32*)(&(init[pc]))) = (u32)&(task_c);
    pc += sizeof(u32);

    *((u32*)(&(init[pc]))) = (u32)run_task;
    pc += sizeof(u32);

    *((u32*)(&(init[pc]))) = (u32)__buf86;
    pc += sizeof(u32);

    pc = 0x100;

    u32 size = (u32)(_binary_init_bin_end - _binary_init_bin_start);
    for (u32 i = 0; i < size; i++)
        init[pc + i] = ((u8*)(_binary_init_bin_start))[i];

    u32 usable = 0xFFFFFFFF;
    for (u16 i = 0; i < 4096; i++)
    {
        tasks[i].size = usable;
        tasks[i].ext = (void*)((u32)i);
    }

    #define SET_ISR(n) interrupts_handle(0x##n, interrupt##n);
    REPEAT256(SET_ISR)

    tasks[0].regs[TEMPEST_REG_PC] = (u32)&(init[pc]);
    for (;;)
    {
        for (task_i = 0; task_i < task_c;)
            run_task(&(tasks[task_i]));
    }
}

static void
run_boot(void)
{
    __load_gdtr();
    __segrel();
    __load_idtr();
}

extern void __attribute__((noreturn))
kernel_main(void)
{
    run_boot();
    run_init();
}
