/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#include "ciel/asm/environ.h"
#include "ciel/utils/types.h"
#include "ciel/utils/screen.h"

#include "ciel/utils/interrupts.h"

__attribute__((packed))
struct idt_entry
{
    u16 offset_lo;
    u16 selector;
    u8 reserved;
    u8 attributes;
    u16 offset_hi;
};

extern void
interrupts_enable(void)
{
    asm volatile ("sti");
    asm volatile ("nop");
}

extern void
interrupts_disable(void)
{
    asm volatile ("cli");
    asm volatile ("nop");
}

extern void
interrupts_handle(u16 val, void *isr)
{
    struct idt_entry idt = {0};
    idt.offset_lo = ((u32)isr) & 0xFFFF;
    idt.selector = 0x8;
    idt.reserved = 0;
    idt.attributes = 0b10001110;
    idt.offset_hi = ((u32)isr) >> 16;

    ((struct idt_entry *)__idt)[val] = idt;
}

extern void __attribute__((interrupt))
interrupt_default(void *unused)
{
    (void)unused;
    screen_bsod("Unhandled interrupt");
}

