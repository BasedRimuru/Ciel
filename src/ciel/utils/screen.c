/*
This file is part of Ciel.

Ciel is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published
by the Free Software Foundation, version 3.

Ciel is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Ciel. If not, see <https://www.gnu.org/licenses/>.
*/

#include "ciel/asm/environ.h"
#include "ciel/utils/types.h"
#include "ciel/utils/interrupts.h"

#include "ciel/utils/screen.h"

#define WIDTH  80
#define HEIGHT 25

#define COLOR_BLACK         0x0
#define COLOR_BLUE          0x1
#define COLOR_GREEN         0x2
#define COLOR_CYAN          0x3
#define COLOR_RED           0x4
#define COLOR_MAGENTA       0x5
#define COLOR_BROWN         0x6
#define COLOR_LIGHT_GREY    0x7
#define COLOR_DARK_GREY     0x8
#define COLOR_LIGHT_BLUE    0x9
#define COLOR_LIGHT_GREEN   0xA
#define COLOR_LIGHT_CYAN    0xB
#define COLOR_LIGHT_RED     0xC
#define COLOR_LIGHT_MAGENTA 0xD
#define COLOR_LIGHT_BROWN   0xE
#define COLOR_WHITE         0xF

static u16 *buffer = (u16*)0xB8000;
static u16 buffer2[80 * 25];

static void
render(void)
{
    for (u16 i = 0; i < 80 * 25; i++)
        buffer[i] = buffer2[i];
}

static void
clear(u16 color)
{
    for (u8 y = 0; y < HEIGHT; y++)
    {
        for (u8 x = 0; x < WIDTH; x++)
            buffer2[(y * WIDTH) + x] = color | ' ';
    }
}

static void
print(char *message, u8 x, u8 y, u16 color)
{
    for (u8 i = 0; message[0] != '\0'; message = &(message[1]))
        buffer2[(y * WIDTH) + x + (i++)] = color | message[0];
}

static void
print_hex(u32 value, u8 x, u8 y, u16 color)
{
    u8 i = 0;
    buffer2[(y * WIDTH) + x + (i++)] = color | '0';
    buffer2[(y * WIDTH) + x + (i++)] = color | 'x';
    while (i < (2 + (sizeof(u32) * (8 / 4))))
    {
        char c = '?';
        u8 v = ((value >> ((32 - 4) - ((i - 2) * 4))) & 0xF);
        if (v < 10)
        {
            c = '0' + v;
        }
        else
        {
            c = 'A' + v - 10;
        }
        buffer2[(y * WIDTH) + x + i++] = color | c;
    }
}

extern void __attribute__((noreturn))
screen_bsod(char *panic)
{
    u16 color = (COLOR_WHITE | COLOR_BLUE << 4) << 8;
    clear(color);

    u8 line = 0;
    char *message = "Ciel: Not enough magicules... Task failed...";
    print(message, 0, line++, color);
    print(panic, 0, line++, color);

    render();

    interrupts_disable();
    for (;;)
        asm volatile ("nop");
}

extern void
screen_debug(struct tempest *task, char *reason)
{
    u16 color = (COLOR_WHITE | COLOR_RED << 4) << 8;
    clear(color);

    u8 line = 0;
    char *message = "Ciel: ";
    print(message, 0, line++, color);
    print(reason, 0, line++, color);

    if (task != NULL)
    {
        line += 15;
        print("Task:", 0, line++, color);
        print_hex((u32)task->ext, 0, line++, color);

        line++;
        print("Task registers:", 0, line++, color);
        for (u8 j = 0; j < 4; j++)
        {
            for (u8 i = 0; i < 5; i++)
                print_hex(task->regs[(j * 5) + i], i * 11, line + j, color);
        }
        line += 5;
    }

    render();
}
